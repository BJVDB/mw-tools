import type { PetTemplate } from '../Data/PetTemplates';
import type { RandomJson } from '../Utils/Random';
import type { GameCondition } from './GameConditionTypes';

type GameActionBase<TType extends string> =
	| {
			type: TType;
			condition?: never;
			else?: never;
	  }
	| {
			type: TType;
			condition: GameCondition;
			else?: GameAction;
	  };

export type GameActionNoop = GameActionBase<'noop'>;

export type GameActionArray = GameActionBase<'array'> & {
	actions: GameAction[];
};

export type GameActionId = GameActionBase<'id'> & {
	id: string;
};

export type GameActionTemplate = GameActionBase<'template'> & {
	template: string;
	params?: Record<string, unknown>;
};

export type GameActionNpcSay = GameActionBase<'npcSay'> &
	(
		| {
				message: string | string[];
				onClose?: GameAction;
				options?: never;
		  }
		| {
				message?: string | string[];
				onClose?: never;
				options: GameActionNpcOption[];
		  }
	);

export type GameActionNpcOption = {
	condition?: GameCondition;
	text: string;
	action?: GameAction;
};

export type GameActionTeleport = GameActionBase<'teleport'> & {
	targetNpcId: number;
};

export type GameActionShop = GameActionBase<'shop'> & {
	// TODO define items here or elsewhere?
};

export type GameActionHeal = GameActionBase<'heal'> & {
	isPerc?: boolean;
	pet?: boolean;
} & (
		| {
				hp: number;
				mp?: number;
		  }
		| {
				hp?: number;
				mp: number;
		  }
	);

export type GameActionExp = GameActionBase<'exp'> & {
	amount: number | RandomJson;
	pet?: boolean;
};

export type GameActionMonster = GameActionBase<'monster'> & {};

export type GameActionQuest = GameActionBase<'quest'> & {
	add?:
		| number
		| {
				quest: number;
				stage?: number;
		  };
	remove?: number;
	set?: {
		quest: number;
		stage: number;
	};
};

export type GameActionAddItem = GameActionBase<'addItem'> & {
	baseItemId: number;
	amount?: number | RandomJson;
};

export type GameActionAddPet = GameActionBase<'addPet'> & {
	pet: string;
};

export type GameActionRemoveItem = GameActionBase<'removeItem'> & {
	baseItemId: number;
	amount?: number | RandomJson;
};

export type GameActionGold = GameActionBase<'gold'> & {
	amount: number | RandomJson;
};

export type GameActionSingle =
	| GameActionNoop
	| GameActionArray
	| GameActionId
	| GameActionTemplate
	| GameActionNpcSay
	| GameActionTeleport
	| GameActionShop
	| GameActionHeal
	| GameActionExp
	| GameActionMonster
	| GameActionQuest
	| GameActionAddItem
	| GameActionAddPet
	| GameActionRemoveItem
	| GameActionGold;

export type GameAction = string | string[] | GameActionSingle | GameActionSingle[];
