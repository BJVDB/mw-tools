import { Logger } from '../../Logger/Logger';
import type { ClientActionContext } from '../GameActionContext';
import { GameConditionExecutable } from '../GameConditionExecutable';
import type { GameConditionTemplate } from '../GameConditionTypes';

export type ConditionTemplateCallback = (
	context: ClientActionContext,
	params?: Record<string, unknown>,
) => boolean;

/**
 * Executes custom conditions.
 */
export class ConditionTemplateExecutable extends GameConditionExecutable<ClientActionContext> {
	protected constructor(
		protected override readonly condition: GameConditionTemplate,
		protected readonly callback: ConditionTemplateCallback,
	) {
		super(condition);
	}

	public static parse(condition: GameConditionTemplate): ConditionTemplateExecutable {
		switch (condition.template) {
			default:
				Logger.error('Unknown condition template', condition);
				throw Error('Unknown condition template ' + condition.template);
		}
	}

	protected run(context: ClientActionContext): boolean {
		return this.callback(context, this.condition.params);
	}
}
