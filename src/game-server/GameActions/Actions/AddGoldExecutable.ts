import { Random } from '../../Utils/Random';
import type { ClientActionContext } from '../GameActionContext';
import { GameActionExecutable } from '../GameActionExecutable';
import type { GameActionGold } from '../GameActionTypes';

/**
 * Give gold to the player.
 */
export class AddGoldExecutable extends GameActionExecutable<ClientActionContext> {
	protected constructor(protected override readonly action: GameActionGold) {
		super(action);
	}

	public static parse(action: GameActionGold): AddGoldExecutable {
		return new this(action);
	}

	protected run(context: ClientActionContext): void {
		let amount = Random.fromJson(this.action.amount);
		context.player.items.addGoldAndSend(amount);
	}
}
