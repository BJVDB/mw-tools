import type { ClientActionContext } from '../GameActionContext';
import { GameActionExecutable } from '../GameActionExecutable';
import type { GameActionSingle, GameActionTeleport } from '../GameActionTypes';

/**
 * Executes a teleport.
 */
export class TeleportExecutable extends GameActionExecutable<ClientActionContext> {
	protected constructor(
		protected override readonly action: GameActionSingle,
		protected readonly targetNpcId: number,
	) {
		super(action);
	}

	public static parse(action: GameActionTeleport): TeleportExecutable {
		return new this(action, action.targetNpcId);
	}

	protected run({ game, player }: ClientActionContext): void {
		let npc = game.npcs.get(this.targetNpcId);

		if (!npc) return;

		let map = npc.mapData.map;

		// TODO: reduce gold
		let point = npc.mapData.point
			.add(-16, -16)
			.toGridPoint()
			.getRandomNearby(5, 10)
			.toMapPoint();

		game.positionManager.onRequestMapChange(player, map, point);
	}
}
