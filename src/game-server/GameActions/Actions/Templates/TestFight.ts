import { monsterTemplates } from '../../../Data/MonsterTemplates';
import { FightType } from '../../../Enums/FightType';
import { Fight } from '../../../GameState/Fight/Fight';
import { FightCreator } from '../../../GameState/Fight/FightCreator';
import type { FightMemberBase } from '../../../GameState/Fight/FightMember';
import { Monster } from '../../../GameState/Monster/Monster';
import { MonsterCreator } from '../../../GameState/Monster/MonsterCreator';
import type { ActionTemplateCallback } from '../ActionTemplateExecutable';

export const testFight: ActionTemplateCallback = ({ player, game }) => {
	let allies: FightMemberBase[] = FightCreator.getParticipants(player);
	let monsters: FightMemberBase[] = [];

	for (let i = 0; i < 3; ++i) {
		let monster = new Monster(MonsterCreator.create(monsterTemplates.teethor));
		monster.id += i;
		monsters.push(monster);
	}

	let fight = new Fight(game, allies, monsters, FightType.Monster);
	fight.start();
};
