export const enum FightMemberType {
	Player = 0,
	Pet = 1,
	Monster = 2,
	Boss = 3,
	Shapeshifted = 4,
}
