export const enum Direction {
	North = 0,
	South = 1,
	West = 2,
	East = 3,
	NorthEast = 4,
	SouthWest = 5,
	NorthWest = 6,
	SouthEast = 7,
}
