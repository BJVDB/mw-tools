import { FightActionCommand } from '../../Enums/FightActionCommand';
import { FightEffect } from '../../Enums/FightEffect';
import { FightType } from '../../Enums/FightType';
import type { ClientActionContext } from '../../GameActions/GameActionContext';
import { createClientContext } from '../../GameActions/GameActionContext';
import type { GameActionExecutable } from '../../GameActions/GameActionExecutable';
import { Logger } from '../../Logger/Logger';
import type { Packet } from '../../PacketBuilder';
import { FightPackets } from '../../Responses/FightPackets';
import { MapPackets } from '../../Responses/MapPackets';
import { PetPackets } from '../../Responses/PetPackets';
import { PlayerPackets } from '../../Responses/PlayerPackets';
import { Point } from '../../Utils/Point';
import { TimedWaiter } from '../../Utils/TimedWaiter';
import type { Game } from '../Game';
import { Monster } from '../Monster/Monster';
import { Player } from '../Player/Player';
import type { FightMemberBase } from './FightMember';
import { FightMember } from './FightMember';

export class Fight {
	/**
	 * Participants in the fight, mapped by ID.
	 */
	public readonly members: Map<number, FightMember> = new Map();

	/**
	 * All players in this fight.
	 */
	public readonly players: Set<Player> = new Set();

	/**
	 * FightMembers on sideA (location index < 10) sorted by highest speed first.
	 */
	private readonly sideA: FightMember[];

	/**
	 * FightMembers on sideB (location index >= 10) sorted by highest speed first.
	 */
	public readonly sideB: FightMember[];

	/**
	 * Waits for players to send FightReady.
	 */
	public readyWaiter: TimedWaiter<Player> | null = null;

	/**
	 * Waits for players to send FightTurnDone;
	 */
	public turnReadyWaiter: TimedWaiter<Player> | null = null;

	/**
	 * Waits for players to send FightEnd.
	 */
	public endWaiter: TimedWaiter<Player> | null = null;

	/**
	 * Action that gets called on every player that wins the fight.
	 */
	public onMapPlayerFightWin: GameActionExecutable<ClientActionContext> | null = null;

	/**
	 * Type of fight so it will only provide rewards on a monster fight
	 */
	private fightType: FightType;

	/**
	 * Winning side
	 */
	public winningSide: number | null = null;

	/**
	 * Initialise a new fight.
	 * @param game
	 * @param sideA
	 * @param sideB
	 */
	public constructor(
		private game: Game,
		sideA: FightMemberBase[],
		sideB: FightMemberBase[],
		fightType: FightType,
	) {
		this.fightType = fightType;

		this.sideA = sideA.map((base, i) => {
			let member = new FightMember(this, base);
			member.location = i;
			this.members.set(base.id, member);
			return member;
		});

		this.sideB = sideB.map((base, i) => {
			let member = new FightMember(this, base);
			member.location = 10 + i;
			this.members.set(base.id, member);
			return member;
		});

		for (let member of this.members.values()) {
			if (member.base instanceof Player) this.players.add(member.base);
		}
	}

	/**
	 * Start the fight.
	 */
	public start(): void {
		this.game.fights.add(this);

		for (let member of this.members.values()) {
			if (member.base instanceof Player) {
				member.base.fightData.currentFight = this;
				member.base.fightStats.update(member.base.fightData, member.base.items.equipment);
			} else {
				member.base.fightStats.update(member.base.fightData);
			}
		}

		this.sortBySpeed();
		this.readyWaiter = new TimedWaiter(() => this.onFightReady(), this.players, 20_000);
		this.sendPacket(FightPackets.start(this));
	}

	/**
	 * Get the members of one side of the fight.
	 * @param member
	 * @param opposite
	 */
	public getSide(member: FightMember, opposite: boolean): Readonly<FightMember[]> {
		let sideA = member.location < 10;

		if (opposite) sideA = !sideA;

		return sideA ? this.sideA : this.sideB;
	}

	/**
	 * Check if player won the fight
	 * @param Player
	 */
	public checkWinner(player: Player): boolean {
		if (this.winningSide) {
			for (let member of this.members.values()) {
				if (player === member.base) {
					if (
						member.location < this.winningSide &&
						member.location >= this.winningSide - 10
					)
						return true;
				}
			}
		}
		return false;
	}

	/**
	 * Sorts the players on each side.
	 */
	private sortBySpeed(): void {
		this.sideA.sort(FightMember.compareSpeed);
		this.sideB.sort(FightMember.compareSpeed);
	}

	/**
	 * Called when all players are ready.
	 */
	private onFightReady(): void {
		this.readyWaiter = null;
		this.sendPacket(FightPackets.go);
		setTimeout(() => this.doNextTurn(this.getNextTurnMember()), 3000);
	}

	/**
	 * Called when all players are done with the turn.
	 */
	private onTurnReady(): void {
		if (this.isOneSideDead()) {
			this.endFight();
			return;
		}

		let turn = this.getNextTurnMember();
		this.sendPacket(FightPackets.turnContinue(turn.base.id));
		setTimeout(() => this.doNextTurn(turn), 1500);
	}

	/**
	 * Called when the fight should be ended.
	 */
	private endFight(): void {
		this.endWaiter = new TimedWaiter(() => this.onClosed(), this.players, 5000);
		this.sendPacket(FightPackets.end);

		for (let player of this.players.values()) {
			if (!player.client) continue;

			player.client.write(
				player.activePet ? PetPackets.attributes(player.activePet) : null,
				...MapPackets.mapData(player),
				MapPackets.npcList(player),
				MapPackets.enter,
				PlayerPackets.information(player),
			);

			// Only provide rewards on monster fights not pvp
			if (this.fightType == FightType.Monster) {
				if (this.checkWinner(player))
					this.onMapPlayerFightWin?.execute(createClientContext(player.client));

				player.onPlayerFightWin?.execute(createClientContext(player.client));
			}

			if (player.fightData.stats.currentHp === 0) {
				let point = new Point(290, 250).toMapPoint();
				let map = this.game.maps.get(1);

				// Leave party
				player.party?.removeMember(player.id);
				if (map) this.game.positionManager.onRequestMapChange(player, map, point);
			}
		}
	}

	/**
	 * Called when all players have called FightClosed.
	 */
	private onClosed(): void {
		for (let player of this.players) player.fightData.currentFight = null;

		this.game.fights.delete(this);
	}

	/**
	 * Execute the next turn.
	 */
	private doNextTurn(member: FightMember): void {
		this.sendPacket(FightPackets.turnPause);
		member.nextTurn += 1 / member.base.fightStats.totals.speed;

		if (member.base instanceof Monster) this.updateMonsterAction(member);

		member.doEffectTurn();
		let result = member.action.execute();

		if (!result) {
			this.onTurnReady();
			return;
		}

		this.turnReadyWaiter = new TimedWaiter(() => this.onTurnReady(), this.players, 5000);
		this.sendPacket(FightPackets.actionResult(result));
	}

	/**
	 * Get the fight member whose turn it is.
	 */
	private getNextTurnMember(): FightMember {
		let members = Array.from(this.members.values());
		let turn = members[0];

		for (let member of members) {
			if (member.effect.has(FightEffect.Dead)) continue;

			if (member.nextTurn < turn.nextTurn) turn = member;
		}

		return turn;
	}

	/**
	 * Update what a monster will do.
	 * @param member
	 */
	private updateMonsterAction(member: FightMember): void {
		let targets = this.getSide(member, true).filter(t => !t.effect.has(FightEffect.Dead));
		let target = targets[Math.floor(Math.random() * targets.length)];

		member.action.target = target;
		// TODO mob skills
		member.action.type = FightActionCommand.Melee;
	}

	/**
	 * Check if all members on any side have died.
	 */
	private isOneSideDead(): boolean {
		let side1Alive = false;
		let side2Alive = false;

		for (let member of this.members.values()) {
			if (member.base.fightData.stats.currentHp === 0) continue;

			if (member.location < 10) side1Alive = true;
			else side2Alive = true;

			if (side1Alive && side2Alive) return false;
		}

		if (side1Alive) {
			this.winningSide = 10;
		} else if (side2Alive) {
			this.winningSide = 20;
		}

		return true;
	}

	/**
	 * Send packet to all players in the fight.
	 * @param packet
	 */
	private sendPacket(packet: Buffer | Packet): void {
		for (let player of this.players) player.client?.write(packet);
	}
}
