import type { NpcJson } from '../../Database/Collections/Npc/NpcJson';
import type { ClientActionContext } from '../../GameActions/GameActionContext';
import { createClientContext } from '../../GameActions/GameActionContext';
import type { GameActionExecutable } from '../../GameActions/GameActionExecutable';
import { GameActionParser } from '../../GameActions/GameActionParser';
import type { GameConnection, PlayerConnection } from '../../Server/Game/GameConnection';
import { Individual } from '../Individual/Individual';
import type { GameMap } from '../Map/GameMap';
import { NpcMapData } from './NpcMapData';

// NPC IDs must be in the 0x80000000 range.
export class Npc extends Individual {
	public fightData: null = null;

	public mapData: NpcMapData;

	public action: GameActionExecutable<ClientActionContext> | null = null;

	public constructor(json: NpcJson, maps: Map<number, GameMap>) {
		super(json);
		this.mapData = new NpcMapData(json, maps);
		this.action = GameActionParser.parse(json.action);
	}

	/**
	 * Called when the client talks to this npc.
	 * @param client
	 */
	public onTalk(client: GameConnection): void {
		if (!client.hasPlayer()) return;

		client.player.memory.activeNpc = this;
		this.action?.execute(createClientContext(client));
	}

	/**
	 * Called when the client closes a dialog window.
	 * @param client
	 * @param option
	 */
	public onCloseDialog(client: PlayerConnection, option: number): void {
		if (client.player.memory.activeNpc !== this) return;

		let npcOption = client.player.memory.npcOptions?.[option];
		client.player.memory.npcOptions = null;
		npcOption?.execute(createClientContext(client));
	}
}
