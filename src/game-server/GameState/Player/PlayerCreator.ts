import { CharacterGender, CharacterRace } from '../../Enums/CharacterClass';
import { Skill } from '../../Enums/Skill';
import { Point } from '../../Utils/Point';
import { Random } from '../../Utils/Random';
import type { IndividualMapDataJson } from '../Individual/IndividualMapData';
import type { SkillsGroupJson } from '../Skills/SkillsGroup';
import type { StatRates } from '../Stats/StatRates';
import type { StatsJson } from '../Stats/Stats';
import type { PlayerJson } from './Player';

// TODO Get last ID + 1 from database
let nextId = 1;

/**
 * Class used for creating a new player.
 */
export abstract class PlayerCreator {
	public static create(race: CharacterRace, gender: CharacterGender, name: string): PlayerJson {
		let id = nextId;
		++nextId;

		return {
			name,
			race,
			gender,
			id,
			file: race * 2 + gender + 1,
			fightData: { stats: this.createStats(race), skills: this.createSkills(race, gender) },
			mapData: this.getStartLocation(),
		};
	}

	private static createStats(race: CharacterRace): StatsJson {
		let rates = this.getStatRates(race);

		return {
			hp: { rate: rates.sta, pointsBase: 1 },
			mp: { rate: rates.int, pointsBase: 1 },
			attack: { rate: rates.str, pointsBase: 1 },
			speed: { rate: rates.agi, pointsBase: 1 },
			currentHp: 0,
			currentMp: 0,
			growthRate: 1,
			unused: 4,
		};
	}

	private static getStatRates(race: CharacterRace): StatRates {
		switch (race) {
			case CharacterRace.Human:
				return { sta: 150, int: 120, str: 120, agi: 120, growthRate: 1 };
			case CharacterRace.Centaur:
				return { sta: 160, int: 130, str: 100, agi: 150, growthRate: 1 };
			case CharacterRace.Mage:
				return { sta: 100, int: 160, str: 100, agi: 120, growthRate: 1 };
			case CharacterRace.Borg:
				return { sta: 150, int: 120, str: 150, agi: 100, growthRate: 1 };
		}
	}

	private static createSkills(race: CharacterRace, gender: CharacterGender): SkillsGroupJson[] {
		let skills: SkillsGroupJson[] = [];

		switch (race) {
			case CharacterRace.Human:
				if (gender == CharacterGender.Male) skills.push({ id: Skill.FrailtyI, exp: 0 });
				else if (gender == CharacterGender.Female)
					skills.push({ id: Skill.PoisonI, exp: 0 });

				skills.push(
					{ id: Skill.ChaosI, exp: 0 },
					{ id: Skill.HypnotizeI, exp: 0 },
					{ id: Skill.StunI, exp: 0 },
				);

				break;
			case CharacterRace.Centaur:
				if (gender == CharacterGender.Male)
					skills.push(
						{ id: Skill.PurgeChaosI, exp: 0 },
						{ id: Skill.MultiShotI, exp: 0 },
					);
				else if (gender == CharacterGender.Female)
					skills.push({ id: Skill.UnStunI, exp: 0 }, { id: Skill.BlizzardI, exp: 0 });

				skills.push({ id: Skill.SpeedI, exp: 0 }, { id: Skill.HealOtherI, exp: 0 });

				break;
			case CharacterRace.Mage:
				if (gender == CharacterGender.Male) skills.push({ id: Skill.FireI, exp: 0 });
				else if (gender == CharacterGender.Female) skills.push({ id: Skill.IceI, exp: 0 });

				skills.push(
					{ id: Skill.EvilI, exp: 0 },
					{ id: Skill.FlashI, exp: 0 },
					{ id: Skill.DeathI, exp: 0 },
				);

				break;
			case CharacterRace.Borg:
				if (gender == CharacterGender.Male) skills.push({ id: Skill.EnhanceI, exp: 0 });
				else if (gender == CharacterGender.Female)
					skills.push({ id: Skill.ProtectI, exp: 0 });

				skills.push(
					{ id: Skill.DrainI, exp: 0 },
					{ id: Skill.ReflectI, exp: 0 },
					{ id: Skill.RepelI, exp: 0 },
				);

				break;
		}

		return skills;
	}

	private static getStartLocation(): IndividualMapDataJson {
		return {
			map: 1,
			direction: Random.int(0, 8),
			point: new Point(475, 250).getRandomNearby(0, 10).toMapPoint(),
		};
	}
}
