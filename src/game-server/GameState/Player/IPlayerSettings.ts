export interface IPlayerSettings {
	pkEnabled: boolean;
	mailEnabled: boolean;
	tradeEnabled: boolean;
	addFriendEnabled: boolean;
	partyEnabled: boolean;
	refuseOtherMessagesEnabled: boolean;
	localChatEnabled: boolean;
	partyChatEnabled: boolean;
	pmChatEnabled: boolean;
	guildChatEnabled: boolean;
	marketChatEnabled: boolean;
	worldChatEnabled: boolean;
}
