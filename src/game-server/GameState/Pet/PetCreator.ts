import type { PetTemplate } from '../../Data/PetTemplates';
import { MonsterCreator } from '../Monster/MonsterCreator';
import { Pet } from './Pet';

// TODO Get last ID + 1 from database
let nextId = 0xc0000001;

/**
 * Class used for creating a new pet.
 */
export abstract class PetCreator {
	public static create(petTemplate: PetTemplate): Pet {
		let id = nextId;
		++nextId;

		let petMonster = MonsterCreator.create(petTemplate);

		let pet = new Pet({
			id: id,
			name: petMonster.name,
			file: petMonster.file,
			fightData: petMonster.fightData,
		});

		pet.fightData.stats.healHp();
		pet.fightData.stats.healMp();
		pet.fightData.stats.unused = 4;
		pet.loyalty = 100;

		return pet;
	}
}
