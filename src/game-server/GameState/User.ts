import { petTemplates } from '../Data/PetTemplates';
import { CharacterGender, CharacterRace } from '../Enums/CharacterClass';
import { EquipmentSlot } from '../Enums/EquipmentSlot';
import { Skill } from '../Enums/Skill';
import type { GameConnection } from '../Server/Game/GameConnection';
import { Item } from './Item/Item';
import { Level } from './Level';
import { Player } from './Player/Player';
import { PlayerCreator } from './Player/PlayerCreator';

export class User {
	public characters: Player[] = [];

	public constructor(public username: string, client: GameConnection) {
		let testProps = PlayerCreator.create(CharacterRace.Human, CharacterGender.Male, '');
		testProps.name = 'Player ' + testProps.id;
		let test = new Player(testProps, client.game);

		test.level = Level.fromLevel(10);
		let { stats, skills, resist } = test.fightData;
		stats.hp.pointsBase = 10;
		stats.mp.pointsBase = 10;
		stats.attack.pointsBase = 10;
		stats.speed.pointsBase = 10;
		stats.unused = 40;
		stats.healHp();
		stats.healMp();

		// Generic stats used for testing purposes
		resist.criticalRate = 10;
		resist.criticalDamage = 0;
		resist.dodgeRate = 25;

		test.items.gold = 5000;
		test.items.bankGold = 10000;

		let item = new Item(client.game.baseItems.get(1)!);
		test.items.equipment.set(EquipmentSlot.Weapon, item);

		let item2 = new Item(client.game.baseItems.get(2)!);
		item2.count = 10;
		test.items.inventory.set(0, item2);

		test.addPet(petTemplates.capilla);
		test.addPet(petTemplates.antEater);
		test.addPet(petTemplates.oceanLoong);

		this.characters.push(test);
	}
}
