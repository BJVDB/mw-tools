import type { Skill } from '../Enums/Skill';

export type SkillData = {
	id: Skill;
	level: number;
	exp: number;
};
