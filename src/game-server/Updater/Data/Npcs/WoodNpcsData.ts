import type { NpcJson } from '../../../Database/Collections/Npc/NpcJson';
import { Direction } from '../../../Enums/Direction';
import { nurseData } from './NurseData';

export const WoodNpcsData: NpcJson[] = [
	{
		id: 0x80000077,
		name: 'Messenger',
		file: 159,
		map: 1,
		point: { x: 1040, y: 920 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000112,
		name: 'Healer (Shop)',
		file: 103,
		map: 1,
		point: { x: 1728, y: 5120 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000124,
		name: 'Messenger',
		file: 159,
		map: 1,
		point: { x: 208, y: 6040 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000135,
		name: 'Weaponsmith',
		file: 159,
		map: 1,
		point: { x: 2288, y: 5744 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000154,
		name: 'Thief',
		file: 109,
		map: 1,
		point: { x: 2720, y: 4920 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000157,
		name: 'Warrior Lisa',
		file: 108,
		map: 1,
		point: { x: 2784, y: 1200 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000160,
		name: 'Death Avian',
		file: 159,
		map: 1,
		point: { x: 2832, y: 5960 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000171,
		name: 'Girl Thief',
		file: 2,
		map: 1,
		point: { x: 3040, y: 1000 },
		direction: Direction.SouthEast,
	},
	{
		id: 0x80000183,
		name: 'Archer George',
		file: 109,
		map: 1,
		point: { x: 3328, y: 3944 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000186,
		name: 'Death Beast',
		file: 159,
		map: 1,
		point: { x: 352, y: 6320 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000191,
		name: 'Hawker',
		file: 104,
		map: 1,
		point: { x: 3712, y: 2736 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000192,
		name: 'Gambler John',
		file: 130,
		map: 1,
		point: { x: 3744, y: 1000 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000194,
		name: 'Translar',
		file: 131,
		map: 1,
		point: { x: 3760, y: 640 },
		direction: Direction.SouthEast,
	},
	{
		id: 0x80000204,
		name: 'Reborn Stats',
		file: 111,
		map: 1,
		point: { x: 4016, y: 440 },
		direction: Direction.SouthEast,
	},
	{
		id: 0x80000206,
		name: 'Buyer',
		file: 129,
		map: 1,
		point: { x: 4064, y: 1960 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000207,
		name: 'Diana',
		file: 122,
		map: 1,
		point: { x: 4112, y: 6800 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000211,
		name: 'Elder of Faith',
		file: 132,
		map: 1,
		point: { x: 4192, y: 496 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000213,
		name: 'RebornGuardian',
		file: 160,
		map: 1,
		point: { x: 4256, y: 680 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000227,
		name: 'Sister Alice',
		file: 159,
		map: 1,
		point: { x: 4768, y: 2600 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000242,
		name: 'Whisperor',
		file: 117,
		map: 1,
		point: { x: 5136, y: 352 },
		direction: Direction.SouthEast,
	},
	{
		id: 0x80000246,
		name: 'Messenger',
		file: 111,
		map: 1,
		point: { x: 5200, y: 368 },
		direction: Direction.SouthEast,
	},
	{
		id: 0x80000248,
		name: 'Death Warrior',
		file: 159,
		map: 1,
		point: { x: 528, y: 1240 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000253,
		name: 'Nurse',
		file: 159,
		map: 1,
		point: { x: 5312, y: 1712 },
		direction: Direction.SouthWest,
		action: nurseData,
	},
	{
		id: 0x80000261,
		name: 'Recycler',
		file: 159,
		map: 1,
		point: { x: 5520, y: 6560 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000262,
		name: 'Defender',
		file: 109,
		map: 1,
		point: { x: 5568, y: 680 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000266,
		name: 'Master Tracker',
		file: 110,
		map: 1,
		point: { x: 5760, y: 1040 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000269,
		name: 'Messenger',
		file: 159,
		map: 1,
		point: { x: 5968, y: 2360 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000274,
		name: 'Death Spirit',
		file: 159,
		map: 1,
		point: { x: 6240, y: 6160 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000278,
		name: 'Buyer',
		file: 129,
		map: 1,
		point: { x: 6400, y: 5200 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000280,
		name: 'Map Merchant',
		file: 119,
		map: 1,
		point: { x: 6512, y: 2400 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000283,
		name: 'Thief Boss',
		file: 126,
		map: 1,
		point: { x: 6640, y: 5520 },
		direction: Direction.NorthEast,
	},
	{
		id: 0x80000285,
		name: 'Death Skeleton',
		file: 159,
		map: 1,
		point: { x: 672, y: 2680 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000287,
		name: 'Soldier Johnny',
		file: 159,
		map: 1,
		point: { x: 672, y: 6160 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000288,
		name: 'Capilla',
		file: 254,
		map: 1,
		point: { x: 6720, y: 1680 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000291,
		name: 'Nurse',
		file: 159,
		map: 1,
		point: { x: 6880, y: 1600 },
		direction: Direction.SouthWest,
		action: nurseData,
	},
	{
		id: 0x80000292,
		name: 'Messenger',
		file: 111,
		map: 1,
		point: { x: 6960, y: 5360 },
		direction: Direction.SouthEast,
	},
	{
		id: 0x80000294,
		name: 'Porter',
		file: 124,
		map: 1,
		point: { x: 7040, y: 1840 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000295,
		name: 'Town Guide',
		file: 159,
		map: 1,
		point: { x: 7120, y: 1440 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000298,
		name: 'Rowdy Tourist',
		file: 159,
		map: 1,
		point: { x: 720, y: 3360 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000299,
		name: 'Stonesmith',
		file: 117,
		map: 1,
		point: { x: 7280, y: 1240 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000300,
		name: 'Battleground',
		file: 159,
		map: 1,
		point: { x: 7280, y: 1840 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000301,
		name: 'Compensator',
		file: 159,
		map: 1,
		point: { x: 7280, y: 4800 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000302,
		name: 'Redemp Angel',
		file: 134,
		map: 1,
		point: { x: 7280, y: 5120 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000303,
		name: 'Unmute',
		file: 159,
		map: 1,
		point: { x: 7360, y: 1600 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000304,
		name: 'Crab Pit',
		file: 120,
		map: 1,
		point: { x: 7360, y: 4440 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000309,
		name: 'Andreen',
		file: 123,
		map: 1,
		point: { x: 7440, y: 4960 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000310,
		name: 'Buyer',
		file: 129,
		map: 1,
		point: { x: 7488, y: 2040 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000311,
		name: 'Ticket Lady',
		file: 159,
		map: 1,
		point: { x: 752, y: 5664 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000312,
		name: 'Capilla',
		file: 254,
		map: 1,
		point: { x: 7520, y: 1240 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000313,
		name: 'Healer',
		file: 159,
		map: 1,
		point: { x: 7520, y: 2560 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000315,
		name: 'Brave Angel',
		file: 134,
		map: 1,
		point: { x: 7680, y: 960 },
		direction: Direction.SouthEast,
	},
	{
		id: 0x80000316,
		name: 'New Horizon',
		file: 159,
		map: 1,
		point: { x: 7760, y: 1200 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000317,
		name: 'Bank Teller',
		file: 113,
		map: 1,
		point: { x: 7760, y: 2800 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000318,
		name: 'Groceror',
		file: 105,
		map: 1,
		point: { x: 7760, y: 4520 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000320,
		name: 'Redeem Steward',
		file: 106,
		map: 1,
		point: { x: 7840, y: 1720 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000321,
		name: 'Citizen Kane',
		file: 101,
		map: 1,
		point: { x: 7840, y: 6200 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000322,
		name: 'Love Angel',
		file: 134,
		map: 1,
		point: { x: 7920, y: 880 },
		direction: Direction.SouthEast,
	},
	{
		id: 0x80000325,
		name: 'Capilla',
		file: 254,
		map: 1,
		point: { x: 8160, y: 1040 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000326,
		name: 'Help Newbie',
		file: 134,
		map: 1,
		point: { x: 8160, y: 2000 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000327,
		name: 'Duke William',
		file: 159,
		map: 1,
		point: { x: 8192, y: 5200 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000328,
		name: 'Carnival',
		file: 159,
		map: 1,
		point: { x: 8240, y: 2240 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000329,
		name: 'Nurse',
		file: 114,
		map: 1,
		point: { x: 8320, y: 3320 },
		direction: Direction.SouthWest,
		action: nurseData,
	},
	{
		id: 0x80000330,
		name: 'Drunkard Fenny',
		file: 159,
		map: 1,
		point: { x: 8480, y: 6320 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000331,
		name: 'Healer (Shop)',
		file: 103,
		map: 1,
		point: { x: 8592, y: 3240 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000338,
		name: 'Ticket Lady',
		file: 159,
		map: 1,
		point: { x: 992, y: 784 },
		direction: Direction.SouthWest,
	},
];
