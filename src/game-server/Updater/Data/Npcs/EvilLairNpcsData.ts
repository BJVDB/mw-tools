import type { NpcJson } from '../../../Database/Collections/Npc/NpcJson';
import { Direction } from '../../../Enums/Direction';

export const EvilLairNpcsData: NpcJson[] = [
	{
		id: 0x80000151,
		name: 'Eyebasher',
		file: 159,
		map: 69,
		point: { x: 1040, y: 880 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000152,
		name: 'Lohan',
		file: 159,
		map: 69,
		point: { x: 1360, y: 1880 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000153,
		name: 'Pet Breeder',
		file: 159,
		map: 68,
		point: { x: 3920, y: 3160 },
		direction: Direction.SouthWest,
	},
];
