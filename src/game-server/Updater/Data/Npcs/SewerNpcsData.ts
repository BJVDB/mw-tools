import type { NpcJson } from '../../../Database/Collections/Npc/NpcJson';
import { Direction } from '../../../Enums/Direction';

export const SewerNpcsData: NpcJson[] = [
	{
		id: 0x80000106,
		name: 'Tour Agent',
		file: 159,
		map: 35,
		point: { x: 400, y: 560 },
		direction: Direction.SouthWest,
	},
];
